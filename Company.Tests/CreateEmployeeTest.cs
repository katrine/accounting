﻿using System;
using Company.Database.Personal;
using NUnit.Framework;

namespace Company.Tests
{
    [TestFixture]
    class CreateEmployeeTest : SetupData
    {
        [Test]
        public void Create_EployeeChiefManager()
        {
            Manager manager = new Manager("Ivan", null, new DateTime(2010,2,10));
            myCompany.AddPersonal(manager, myCompany);
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013,2,20));
            string result = myCompany.AddPersonal(employee, myCompany, manager);
            Assert.AreEqual("ok", result);
        }

        [Test]
        public void Create_EployeeChiefSales()
        {
            Sales sales = new Sales("Ivan Sales", null, new DateTime(2010, 2, 10));
            myCompany.AddPersonal(sales, myCompany);
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013, 2, 20));
            string result = myCompany.AddPersonal(employee, myCompany, sales);
            Assert.AreEqual("ok", result);
        }

        [Test]
        [ExpectedException( "System.ApplicationException", ExpectedMessage = "Error subordination")]
        public void Should_ThrowException_When_ChiefIsEmployee()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            Employee employee2 = employee;
            myCompany.AddPersonal(employee2, myCompany, employee);
        }

        [Test]
        [ExpectedException("System.ApplicationException", ExpectedMessage = "Employee can't have any subordinate person")]
        public void Should_ThrowException_When_AddEployeeToEmployee()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            Employee employee2 = employee;
            myCompany.AddPersonal(employee2, myCompany);
            employee.AddEmployee(employee2);
        }

        [Test]
        public void Salary0YearWorksEployee()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            double salary = employee.CountSalary();
            Assert.AreEqual(2000, salary);
        }

        [Test]
        public void Salary1YearWorksEployee()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2012, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            double salary = employee.CountSalary();
            Assert.AreEqual(2060, salary);
        }

        [Test]
        public void Salary13YearWorksEployee() // more than max % 
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2000, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            double salary = employee.CountSalary();
            Assert.AreEqual(2600, salary);
        }

        [Test]
        public void Salary_NotWorksDateTime_Eployee()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2012, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            double salary = employee.CountSalary(new DateTime(2012, 2, 19));
            Assert.AreEqual(0, salary);
        }
    }
}
