﻿using System;
using Company.Database.Personal;
using NUnit.Framework;

namespace Company.Tests
{
    [TestFixture]
    class CompanyTests : SetupData
    {
        [Test]
        public void CountOfEployees()
        {
            Assert.AreEqual(5, myCompany.Staff.Count);
        }

        [Test]
        public void CompanySalary()
        {
            Assert.AreEqual(10949.36, myCompany.CountSalaryCompany());
        }

        [Test]
        public void CreateChef_ManagerAboveSales()
        {
            Sales sales = new Sales("Nikita", null);
            myCompany.AddPersonal(sales, myCompany);
            Manager manager = new Manager("Ivan", null, new DateTime(2010,2,20));
            myCompany.AddPersonal(manager, myCompany);
            sales.AddChief(manager);
        }

        [Test]
        public void CreateChef_ManagerAboveEmployee()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013,2,20));
            myCompany.AddPersonal(employee, myCompany);
            Manager manager = new Manager("Ivan", null, new DateTime(2010,2,20));
            myCompany.AddPersonal(manager, myCompany);
            employee.AddChief(manager);
        }

        [Test]
        [ExpectedException("System.ApplicationException", ExpectedMessage = "Error subordination")]
        public void CreateChef_EmployeeAboveManager()
        {
            Employee employee = new Employee("IvanEmployee", null, new DateTime(2013, 2, 20));
            myCompany.AddPersonal(employee, myCompany);
            Manager manager = new Manager("Ivan", null, new DateTime(2010, 2, 20));
            myCompany.AddPersonal(manager, myCompany);
            manager.AddChief(employee);
        }
    }
}
