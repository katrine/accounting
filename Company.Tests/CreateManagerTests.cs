﻿using System;
using Company.Database.Personal;
using NUnit.Framework;


namespace Company.Tests
{
    [TestFixture]
    class CreateManagerTests : SetupData
    {
        [Test]
        public void Salary0YearWorksManager_2LevelsSubordinate()
        {
            Manager manager = new Manager("Ivan", null, new DateTime(2012, 9, 20));
            myCompany.AddPersonal(manager, myCompany);
            Manager manager1 = new Manager("Ivan manager", manager, new DateTime(2013, 1, 20));
            myCompany.AddPersonal(manager1, myCompany, manager);
            Assert.AreEqual(2000, manager1.CountSalary());
            Employee employee = new Employee("Ivan Employee", manager1, new DateTime(2000, 1, 2));
            myCompany.AddPersonal(employee, myCompany, manager1);
            Assert.AreEqual(2013, manager1.CountSalary());
            Assert.AreEqual(2600, employee.CountSalary());
            double salary = manager.CountSalary();
            Assert.AreEqual(2010.065, salary);
        }

        [Test]
        public void Salary_5YearWorksManager_SubordinateEmployees()
        {
            Manager manager = new Manager("Ivan", null,  new DateTime(2007, 9, 20));
            myCompany.AddPersonal(manager, myCompany);
            Employee employee = new Employee("IvanEmployee", manager,  new DateTime(2013, 2, 20));
            myCompany.AddPersonal(employee, myCompany, manager);
            // employee for 0 years
            Assert.AreEqual(2000, employee.CountSalary());
            Employee employee1 = new Employee("IvanEmployee1", manager,  new DateTime(2010, 1, 2));
            myCompany.AddPersonal(employee1, myCompany, manager);
            // employee for 3 years
            Assert.AreEqual(2180, employee1.CountSalary());
            // manager salary
            Assert.AreEqual(2520.9, manager.CountSalary());
        }

        [Test]
        public void Salary_5YearWorksManager_SubordinateEmployeeAndManager()
        {
            Manager manager = new Manager("Ivan", null, new DateTime(2007, 9, 20));
            myCompany.AddPersonal(manager, myCompany);
            Employee employee = new Employee("IvanEmployee", manager, new DateTime(2013, 1, 20));
            myCompany.AddPersonal(employee, myCompany, manager);
            // employee for 0 years
            Assert.AreEqual(2000, employee.CountSalary());
            Manager employeeManager = new Manager("KirillManager", manager, new DateTime(2012, 1, 2));
            myCompany.AddPersonal(employeeManager, myCompany, manager);
            // manager for 1 year
            Assert.AreEqual(2100, employeeManager.CountSalary());
            // manager salary
            Assert.AreEqual(2520.5, manager.CountSalary());
        }

        [Test]
        public void Salary20YearWorksManager()
        {
            Manager manager = new Manager("Ivan", null, new DateTime(1996, 3, 25));
            myCompany.AddPersonal(manager, myCompany);
            Assert.AreEqual(2800.0, manager.CountSalary());
        }

        [Test]
        public void Salary_AnyYearWorksManager_SubordinateEmployeeAndManager()
        {
            Manager manager = new Manager("Ivan", null, new DateTime(2007, 3, 20));
            myCompany.AddPersonal(manager, myCompany);
            Employee employee = new Employee("IvanEmployee", manager, new DateTime(2000, 1, 20));
            myCompany.AddPersonal(employee, myCompany, manager);
            Manager employeeManager = new Manager("KirillManager", manager, new DateTime(2012, 1, 2));
            myCompany.AddPersonal(employeeManager, myCompany, manager);
            // manager salary
            Assert.AreEqual(0, manager.CountSalary(new DateTime(2006, 4, 1)));
        }
    }
}
