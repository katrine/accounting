﻿using System;
using Company.Database.Personal;
using CompanyNamespace = Company.Database.Company;

namespace Company.Tests
{
    public class SetupData
    {
        protected CompanyNamespace.Company myCompany;

        public SetupData()
        {
            myCompany = new CompanyNamespace.Company();
            Sales sales = new Sales("Nikita", null);
            myCompany.AddPersonal(sales, myCompany);
            Manager manager = new Manager("Ivan", null, new DateTime(2010, 10, 10));
            myCompany.AddPersonal(manager, myCompany);
            Employee employee = new Employee("IvanEmployee", manager, new DateTime(2013,2,20));
            myCompany.AddPersonal(employee, myCompany, manager);
            Employee employee1 = new Employee("IvanEmployee1", manager, new DateTime(2000,01,02));
            myCompany.AddPersonal(employee1, myCompany, manager);
            Employee employee2 = new Employee("IvanEmployee2", sales, new DateTime(2011,2,22));
            myCompany.AddPersonal(employee2, myCompany, sales);
        }
    }
}
