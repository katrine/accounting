﻿using System;
using Company.Database.Personal;
using NUnit.Framework;

namespace Company.Tests
{
    [TestFixture]
    class CreateSales : SetupData
    {
        [Test]
        public void CreateSalesWithChiefManager()
        {
            Manager manager = new Manager("Ivan", null, new DateTime(2013, 3, 20));
            myCompany.AddPersonal(manager, myCompany);
            Sales sales = new Sales("Petro", manager, new DateTime(2013, 2, 20));
            Assert.AreEqual("ok", myCompany.AddPersonal(sales, myCompany, manager));
        }
        
        [Test]
        public void Salary0YearWorksSales()
        {
            Sales sales = new Sales("Ivan", null, new DateTime(2013, 3, 20));
            myCompany.AddPersonal(sales, myCompany);
            Assert.AreEqual(2000, sales.CountSalary());
        }

        [Test]
        public void Salary6YearWorksSales()
        {
            Sales sales = new Sales("Ivan", null, new DateTime(2007, 1, 20));
            myCompany.AddPersonal(sales, myCompany);
            Assert.AreEqual(2120, sales.CountSalary());
        }

        [Test]
        public void Salary6YearWorksSalesWhithEmployee()
        {
            Sales sales = new Sales("Ivan", null, new DateTime(2007, 1, 20));
            myCompany.AddPersonal(sales, myCompany);
            Manager manager = new Manager("Ivan", null, new DateTime(2012, 9, 20));
            myCompany.AddPersonal(manager, myCompany, sales);
            Manager manager2 = new Manager("Ivan_2", null, new DateTime(2012, 1, 20));
            myCompany.AddPersonal(manager2, myCompany, sales);
            Assert.AreEqual(2100, manager2.CountSalary());      // salary manager2 (1 year work)
            Employee employee = new Employee("IvanEmployee", manager, new DateTime(2010, 2, 20));
            myCompany.AddPersonal(employee, myCompany, manager);
            Assert.AreEqual(2180, employee.CountSalary());      // salary employee (3 years work)
            Assert.AreEqual(2010.9, manager.CountSalary());     // salary manager (0 years work)
            Assert.AreEqual(2138.8727, sales.CountSalary());    // salary sales
        }

        [Test]
        public void Salary1YearWorksSalesWhithEmployee()
        {
            Sales sales2 = new Sales("Ivan_2", null, new DateTime(2012, 1, 20));
            myCompany.AddPersonal(sales2, myCompany); 
            Sales sales = new Sales("Ivan", null, new DateTime(2011, 1, 20));
            myCompany.AddPersonal(sales, myCompany, sales2);
            Assert.AreEqual(2040, sales.CountSalary());    // salary sales
            Manager manager = new Manager("Ivan", null, new DateTime(2012, 9, 20));
            myCompany.AddPersonal(manager, myCompany, sales);
            Employee employee = new Employee("IvanEmployee", manager, new DateTime(2010, 2, 20));
            myCompany.AddPersonal(employee, myCompany, manager);
            Assert.AreEqual(2180, employee.CountSalary());      // salary employee (3 years work)
            Assert.AreEqual(0, employee.SumAllLevelsSubordinate);
            Assert.AreEqual(2010.9, manager.CountSalary());     // salary manager (0 years work)
            Assert.AreEqual(2180, manager.SumAllLevelsSubordinate);
            Assert.AreEqual(2052.5727, sales.CountSalary());    // salary sales (2 years work)
            Assert.AreEqual(4190.9, sales.SumAllLevelsSubordinate);
            Assert.AreEqual(2038.7304181, sales2.CountSalary());    // salary sales
            Assert.AreEqual(6243.4727, sales2.SumAllLevelsSubordinate);
        }
    }
}
