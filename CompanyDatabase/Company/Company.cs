﻿using System;
using System.Collections.Generic;
using System.Linq;
using Company.Database.Enums;
using Company.Database.Personal;

namespace Company.Database.Company
{
    public class Company
    {
        /// <summary>
        /// Base value of the salary
        /// </summary>
        public int Salary;

        /// <summary>
        /// Current number of the last added person
        /// </summary>
        private int _TabelNumber;

        public Dictionary<string, double> PaySettings = new Dictionary<string, double>();

        /// <summary>
        /// List of the personal in company. All company staff.
        /// </summary>
        public List<Personal.Personal> Staff { get; set; }

        public Company()
        {
            Salary = 2000;

            PaySettings.Add("EmployeeBase", 3);
            PaySettings.Add("EmployeeMax", 30);
            PaySettings.Add("ManagerBase", 5);
            PaySettings.Add("ManagerMax", 40);
            PaySettings.Add("ManagerSubordirary", 0.5);
            PaySettings.Add("SalesBase", 1);
            PaySettings.Add("SalesMax", 35);
            PaySettings.Add("SalesSubordirary", 0.3);

            _TabelNumber = 0;
            Staff = new List<Personal.Personal>();
        }

        /// <summary>
        ///  Create personal with chief
        /// </summary>
        /// <param name="personalObject">Persona, which is added</param>
        /// <param name="company">Company in which the Persona is added</param>
        /// <param name="chief">Head of person</param>
        /// <returns>"ok" or ApplicationExeption</returns>
        public string AddPersonal(Personal.Personal personalObject, Company company, Personal.Personal chief)
        {
            AddPersonal(personalObject, company);       // create personal without chief

            try
            {
                personalObject.AddChief(chief);         // set chef for personal
                chief.AddEmployee(personalObject);      // add enployee to the chief

                return "ok";
            }
            catch (ApplicationException exception)
            {
                throw (exception);
                //return exception.Message;
            }
        }

        /// <summary>
        ///  Create personal without chief
        /// </summary>
        /// <param name="personalObject">Persona, which is added</param>
        /// <param name="company">Company in which the Persona is added</param>
        public void AddPersonal(Personal.Personal personalObject, Company company) // create employee
        {
            _TabelNumber++;
            personalObject.SetIdentificationValue(_TabelNumber, company);      // set main property for Persona
            Staff.Add(personalObject);                  // add to the list of company staff
        }

        public double CountSalaryCompany()
        {
            return Staff.Sum(personal => personal.CountSalary());
        }
    }
}
