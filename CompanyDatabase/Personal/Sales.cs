﻿using System;
using System.Collections.Generic;
using System.Linq;
using Company.Database.Enums;
using Company.Database.Helpers;

namespace Company.Database.Personal
{
    public class Sales : Personal
    {
        public List<Personal> SubordinatePerson;

        public Sales(string name, Personal chief)
            : base(name, chief)
        {
            SubordinatePerson = new List<Personal>();
            Rank = EmployeeRank.Sales;
        }

        public Sales(string name, Personal chief, DateTime startWork)
            : base(name, chief, startWork)
        {
            SubordinatePerson = new List<Personal>();
            Rank = EmployeeRank.Sales;
        }

        public override void AddEmployee(Personal personalObject)
        {
            SubordinatePerson.Add(personalObject);
        }

        protected override double calculationAddingSalary(int div)
        {
            try
            {
                double salesBase = Company.PaySettings["SalesBase"];
                double salesMax = Company.PaySettings["SalesMax"];

                return salesMax > div * salesBase ? (1 + div * salesBase * 0.01) : (1 + salesMax * 0.01);
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException("Error during read values from dictionary !!!");
            }
        }

        public override double CountSalary()
        {
           try
            {
                // account salary of employees
                double salesSubordinary = 0.01 * Company.PaySettings["SalesSubordirary"];
                SumAllLevelsSubordinate = SubordinatePerson.Sum(personal => personal.SumAllLevelsSubordinate) +
                                            SubordinatePerson.Sum(personal => personal.CountSalary());

                //account his part of the salary + % of his employees
                return Company.Salary * calculationAddingSalary(StartWork.CountPastYears()) + SumAllLevelsSubordinate * salesSubordinary;
            }
            catch (ApplicationException e)
            {
                throw (e);
            }
        }

        public override double CountSalary(DateTime dateAccount)
        {
            try
            {
                int div = StartWork.CountPastYears(dateAccount);

                if (div < 0)
                {
                    SumAllLevelsSubordinate = 0;
                    
                    return 0;
                }
                
                // account salary of employees
                double salesSubordinary = 0.01 * Company.PaySettings["SalesSubordirary"];
                SumAllLevelsSubordinate = SubordinatePerson.Sum(personal => personal.SumAllLevelsSubordinate) +
                                            SubordinatePerson.Sum(personal => personal.CountSalary(dateAccount));

                //account his part of the salary + % of his employees
                return Company.Salary * calculationAddingSalary(div) + SumAllLevelsSubordinate * salesSubordinary;
            }
            catch (ApplicationException e)
            {
                throw (e);
            }
        }
    }
}
