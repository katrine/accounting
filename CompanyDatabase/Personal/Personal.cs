﻿using System;
using Company.Database.Enums;

namespace Company.Database.Personal
{   
    public abstract class Personal
    {
        public int IdPerson;

        public Company.Company Company { get; set; }

        private string _Name;

        protected DateTime StartWork;   // date of begin work

        protected Personal Chief;       // leader of the person

        protected EmployeeRank Rank;       // rank person : employee, manager, sales

        public double SumAllLevelsSubordinate;

        protected Personal(string name, Personal chief)
        {
            _Name = name;
            Chief = chief;
            StartWork = DateTime.Today;
        }

        protected Personal(string name, Personal chief, DateTime startWork)
        {
            _Name = name;
            Chief = chief;
            StartWork = (DateTime.Today < startWork) ? DateTime.Today : startWork;
        }

        public abstract double CountSalary();

        public abstract double CountSalary(DateTime dateAccount);

        public abstract void AddEmployee(Personal employee);

        /// <summary>
        /// Calculation of surcharges employee
        /// </summary>
        /// <param name="div">Count years of his work.</param>
        /// <returns>Calculated coefficient</returns>
        protected abstract double calculationAddingSalary(int div);

        /// <summary>
        /// Add Chief for Personal
        /// </summary>
        /// <param name="chief">future chief of the personal</param>
        public void AddChief(Personal chief)
        {
            if (chief.Rank != EmployeeRank.Employee)
            {
                Chief = chief;
            }
            else
            {
                //"You can not create this kind of subordination, level of leader below the level of employee";
                throw new ApplicationException("Error subordination");
            }
        }

        /// <summary>
        /// Set value, that identificate personal in the company
        /// </summary>
        /// <param name="tabelNumber">Tabel number, us id-person</param>
        /// <param name="company">Company, for back-link</param>
        public void SetIdentificationValue (int tabelNumber, Company.Company company)
        {
            IdPerson = tabelNumber;     // Set tabel number for person. It's his identification in the company.
            Company = company;          // Back-link for personal whith his company
        }
    }
}
