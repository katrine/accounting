﻿using System;
using System.Collections.Generic;
using Company.Database.Enums;
using Company.Database.Helpers;

namespace Company.Database.Personal
{
    public class Employee : Personal
    {
        public Employee(string name, Personal chief, DateTime startTime)
            : base(name, chief, startTime)
        {
            Rank = EmployeeRank.Employee;
        }

        protected override double calculationAddingSalary(int div)
        {
            try
            {
                double employeeBase = Company.PaySettings["EmployeeBase"];
                double employeeMax = Company.PaySettings["EmployeeMax"];
            
                return employeeMax > div * employeeBase ? (1 + div * employeeBase * 0.01) : (1 + employeeMax * 0.01);
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException("Error during read values from dictionary !!!");
            }
        }
        
        public override double CountSalary()
        {
            try
            {
                // account salary
                return Company.Salary * calculationAddingSalary(StartWork.CountPastYears());
            }
            catch (ApplicationException e)
            {               
                throw(e);
            }
        }

        public override double CountSalary(DateTime dateAccount)
        {
            try
            {
                // account salary
                int div = StartWork.CountPastYears(dateAccount);
                
                return div < 0 ? 0 : Company.Salary * calculationAddingSalary(div);
            }
            catch (ApplicationException e)
            {
                throw (e);
            }
        }

        /// <summary>
        /// Adding subordinate person.
        /// </summary>
        /// <param name="employee">Add person.</param>
        public override void AddEmployee(Personal employee)
        {
            throw new ApplicationException("Employee can't have any subordinate person");
        }
    }
}
