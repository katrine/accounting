﻿using System;
using System.Collections.Generic;
using System.Linq;
using Company.Database.Enums;
using Company.Database.Helpers;

namespace Company.Database.Personal
{
    public class Manager : Personal
    {
        public List<Personal> SubordinateEmployee;

        public Manager(string name, Personal chief) : base (name, chief)
        {
            SubordinateEmployee = new List<Personal>();
            Rank = EmployeeRank.Manager;
        }

        public Manager(string name, Personal chief, DateTime startWork)
            : base(name, chief, startWork)
        {
            SubordinateEmployee = new List<Personal>();
            Rank = EmployeeRank.Manager;
        }

        public override double CountSalary(DateTime dateAccount)
        {
            try
            {
                int div = StartWork.CountPastYears(dateAccount);

                if(div<0)
                {
                    SumAllLevelsSubordinate = 0;

                    return 0;
                }

                // account salary of employees
                double managerSubordinary = 0.01 * Company.PaySettings["ManagerSubordirary"];
                double sumSubordinateLevel = SubordinateEmployee.Sum(personal => personal.CountSalary(dateAccount));

                SumAllLevelsSubordinate = SubordinateEmployee.Sum(personal => personal.SumAllLevelsSubordinate);
                SumAllLevelsSubordinate += sumSubordinateLevel;

                //account his part of the salary + % of his employees 
                return Company.Salary * calculationAddingSalary(div) + sumSubordinateLevel * managerSubordinary;
            }
            catch (ApplicationException e)
            {
                throw (e);
            }
        }

        public override void AddEmployee(Personal personalObject)
        {
            SubordinateEmployee.Add(personalObject);
        }

        protected override double calculationAddingSalary(int div)
        {
            try
            {
                double managerBase = Company.PaySettings["ManagerBase"];
                double managerMax = Company.PaySettings["ManagerMax"];

                return managerMax > div * managerBase ? (1 + div * managerBase * 0.01) : (1 + managerMax * 0.01);
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException("Error during read values from dictionary !!!"); 
            }
        }

        public override double CountSalary()
        {
           try
            {
               // account salary of employees
               double managerSubordinary = 0.01 * Company.PaySettings["ManagerSubordirary"];
               double sumSubordinateLevel = SubordinateEmployee.Sum(personal => personal.CountSalary());
                
               SumAllLevelsSubordinate = SubordinateEmployee.Sum(personal => personal.SumAllLevelsSubordinate);
               SumAllLevelsSubordinate += sumSubordinateLevel;

                //account his part of the salary + % of his employees 
                return Company.Salary * calculationAddingSalary(StartWork.CountPastYears()) + sumSubordinateLevel * managerSubordinary;
            }
            catch (ApplicationException e)
            {               
                throw (e);
            }
        }
    }
}
