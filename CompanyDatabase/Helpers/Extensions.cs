﻿using System;

namespace Company.Database.Helpers
{
    public static class Extensions
    {
        /// <summary>
        /// Count years between current years and dateStart.
        /// </summary>
        /// <param name="dateStart">Date of the person begin work.</param>
        /// <returns>Count years from dateStart.</returns>
        public static int CountPastYears(this DateTime dateStart)
        {
            return DateTime.Today.Subtract(dateStart).Days / 365;
        }

        /// <summary>
        /// Count years between dateEnd and dateStart.
        /// </summary>
        /// <param name="dateStart">Date of the person begin work.</param>
        /// <param name="dateEnd">End of the period. </param>
        /// <returns>count years between dateEnd and dateStart.</returns>
        public static int CountPastYears(this DateTime dateStart, DateTime dateEnd)
        {
            int a = dateEnd.Subtract(dateStart).Days;
            return a < 0 ? a : a/365;
        }
    }
}
