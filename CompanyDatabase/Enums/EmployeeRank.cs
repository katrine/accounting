﻿namespace Company.Database.Enums
{
    /// <summary>
    /// This is enum for rank of employee.
    /// </summary>
    public enum EmployeeRank
    {
        /// <summary>
        /// Person can't has subordinate personal.
        /// </summary>
        Employee,

        /// <summary>
        /// Person has subordinate personal. Middle level.
        /// </summary>
        Manager,

        /// <summary>
        /// Person has subordinate personal. The highest level.
        /// </summary>
        Sales
    }
}
